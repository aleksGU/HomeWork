/*
Напишите функцию isEmpty(obj), которая возвращает true, если у объекта нет свойств, иначе false.


const object = {
    
}

function isEmpty(obj) {
    for (var element in object) {
        return console.log(true);
    }
    return console.log(false);
}

isEmpty(object);


Функция ggg принимает 2 параметра: анонимную функцию, которая возвращает 3 и анонимную функцию, которая
возвращает 4. Верните результатом функции ggg сумму 3 и 4.


function ggg(Anon1, Anon2) {
    return console.log(f1() + f2());
}

function f1() {
    return 3
}

function f2() {
    return 4
}

ggg(f1, f2); 

Создать объект "Документ", в котором определить свойства "Заголовок, тело, футер, дата". 
Создать в объекте вложенный объект - "Приложение". 
Создать в объекте "Приложение", вложенные объекты, "Заголовок, тело, футер, дата". 
Создать методы для заполнения и отображения документа.
*/

const docyment = {
    header: "",
    body: "",
    footer: "",
    data: "",
    
    application: {
        header: "",
        body: "",
        footer: "",
        data: "",
    },

    zapoln: function () {
        docyment.header = prompt("Введіть заголовок документа");
        docyment.body = prompt("Введіть тіло документа");
        docyment.footer = prompt("Введіть футер документа");
        docyment.data = prompt("Введіть дату документа");

        docyment.application.header = prompt("Введіть заголовок додатка");
        docyment.application.body = prompt("Введіть тіло додатка");
        docyment.application.footer = prompt("Введіть футер додатка");
        docyment.application.data = prompt("Введіть дату додатка"); 
    }, 

    show: function () {
        document.write("<p>Заголовок документа: " + docyment.header);
        document.write("<p>Тіло документа: " + docyment.body);
        document.write("<p>Футер документа: " + docyment.footer);
        document.write("<p>Дата документа: " + docyment.data);

        document.write("<hr/><p>Заголовок додатка: " + docyment.application.header);
        document.write("<p>Тіло додатка: " + docyment.application.body);
        document.write("<p>Футер додатка: " + docyment.application.footer);
        document.write("<p>Дата додатка: " + docyment.application.data);
    }
}

docyment.zapoln();
docyment.show();