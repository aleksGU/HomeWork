/*
    Создайте массив размерностью 15 элементов, выведите все элементы в обратном порядке 
и разделите каждый элемент спецсимволом "Облака". При загрузке страницы спросите у 
пользователя индекс и удалите этот элемент из массива.
*/
var k = 1;
const arr = new Array(15);


for (a = 0; a < 15; a++) {
    arr[a] = k++;
    // document.write(arr[a] + '<br>');
}

document.write(arr);

arr.reverse();

document.write("<br><br>" + arr);

var cloud = arr.join(" &#9729; ");

document.write("<br><br>" + cloud);

document.write("<br><br>");

resultFew = "Мало";
resultMuch = "Забагато";
        
var del = prompt("Введіть значення від 1 до 15 для видалення значення з масиву");

if (del < 1) {
    document.write(resultFew);
} else if (del > 15) {
    document.write(resultMuch);
} else {

    del -= 1;

    let delelm = arr.splice(del, 1);

    document.write("Видалений елемент:" + delelm);
}