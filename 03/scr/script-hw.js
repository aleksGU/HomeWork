/*
    1. Создайте массив styles с элементами «Джаз» и «Блюз».
    2. Добавьте «Рок-н-ролл» в конец.
    3. Замените значение в середине на «Классика». 
    3.1. Ваш код для поиска значения в середине должен работать для массивов с любой длиной.
    4. Удалите первый элемент массива и покажите его.
    5. Вставьте «Рэп» и «Регги» в начало массива.
*/

var styles = ['Джаз', 'Блюз'];

document.write(styles);

styles = styles.concat('Рок-н-ролл');

document.write("<br><br>" + styles);

middleMassive = Math.floor(styles.length / 2);

styles.splice(middleMassive, 1, 'Классика');

document.write("<br><br>" + styles);

// #4
let deleteFirst = styles.splice(0, 1);

document.write("<br><br>Видалений елемент:" + deleteFirst);

// #5

styles.unshift('Рэп', 'Регги');

document.write("<br><br>Додані елементи: " + styles);