/* Задача 1. Напишите функцию, которая принимает произвольное количество аргументов 
в виде строк и выводит их через пробел с помощью document.write() в тело html-документа. 
Например, функция  showWords("I've", "been", "learning", "JavaScript", "for", "a", "month") 
должна вывести фразу "I've been learning JavaScript for a month".
*/

function showWords (words) {
    let arr = Array.from(arguments);    
    arr = arr.join(" ");
    document.write(arr);
    }

    showWords("I've", "been", "learning", "JavaScript", "for", "a", "month");

    document.write("<hr />");


/* Задача 2. Напишите функцию, которая принимает произвольное количество аргументов 
в виде однозначных чисел и возвращает 1 многозначное число. 
Например, функция с именем complexDigit() при вызове  complexDigit(3,6,7) 
вернет число 367, а complexDigit(1,9, 4, 8,3) вернет число 19483. */

function complexDigit (a) {
/*
    const arr3 = [];

    for (var i = 0; i < arguments.length; i++) {
        arr3.push(arguments[i]);
    }
*/
    let arr = Array.from(arguments);
    
    arr = arr.join("");

    document.write(arr, "<br>");

    }

complexDigit(3, 6, 7);
complexDigit(1, 9, 4, 8, 3);
