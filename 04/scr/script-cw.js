/* КАЛЬКУЛЯТОР

За каждую операцию будет отвечать отдельная функция,
т.е. для сложения - add(a,b), для умножения - multiple(a,b) и т.д.
Каждая из них принимает в аргументы только два числа и возвращает результат операции над двумя числами
Если число не передано в функцию аргументом - ПО УМОЛЧАНИЮ присваивать этому аргументу 0.

Основная функция calculate()
Принимает ТРИ АРГУМЕНТА:
    1 - число
    2 - число
    3 - функция которую нужно выполнить для двух этих чисел.
    Таким образом получается что основная функция калькулятор будет вызывать переданную ей аргументом 
    функцию для двух чисел, которые передаются остальными двумя аргументами. При делении на 0 выводить ошибку.
    Функия калькулятор доджна принять на вход 3 аругмента, Если аргументов больше или меньше выводить ошибку.
                                        
*/

let a, b, diya = " ", result;

if (a == undefined || b == undefined) {
    a = 0; b = 0;
}

function calculate () {
     a = prompt("Введіть перше значення");
     b = prompt("Введіть друге значення");
     diya = prompt("Введіть знак операції");

    a = parseInt(a);
    b = parseInt(b);
    
    if (diya == "+") {     
        result = summ(a, b);
    } else if (diya == "-") {
        result = subtraction(a, b);
    } else if (diya == "*") {
        result = multiple(a, b);
    } else if (diya == "/") {
        result = division(a, b);
    } else {
        document.write("<p><b style='color:red'> Невірний знак операції </p>");
    }

    function summ(a, b) {
        return a + b;
    }

    function subtraction(a, b) {
        return a - b;
    }

    function multiple(a, b) {
        return a * b;
    }

    function division(a, b) {
        if (a == 0 || b == 0) {
            document.write("<p><b style='color:red'> Error! </p>");
        } else {
            return a / b;
        }
    }
}


calculate();

document.write("Відповідь: ", result);
